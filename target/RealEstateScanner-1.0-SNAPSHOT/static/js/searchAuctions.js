$(document).ready(function($) {

    $("#search-form").submit(function(event) {

        // Disble the search button
        enableSearchButton(false);

        // Prevent the form from submitting via the browser.
        event.preventDefault();

        searchViaAjax();

    });

});

function searchViaAjax() {
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;

    var search = {}
    search["startDate"] = $("#startDate").val();
    search["endDate"] = $("#endDate").val();

    $.ajax({
        headers: headers,
        url : 'ajaxSearch',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data : JSON.stringify(search),
        timeout : 100000,
        success: function (data) {
            //var returnedData = JSON.parse(data);
            var trHTML = '';
            var checkbox = document.createElement("INPUT");
            checkbox.type = "checkbox";
            $.each(data, function (i, item) {
                trHTML += '<tr><td>' + item.auctionDate + '</td><td>'
                    + '<a href="'+item.url+'">'+item.auctionDescription + '</a></td><td>'
                    +item.startPrice +'</td><td class="hidden">'
                    +item.sso +'</td><td class="hidden">' +
                    item.url + '</td><td class="bs-checkbox ">' +
                    '<input data-index='+i+' name="btSelectItem" type="checkbox" value='+i+'></td></tr>';
            });
            $('#records_table').empty().append(trHTML);
            $('#tableData').paging({limit:5})
        },
        error : function(e) {
            console.log("ERROR: ", e);
            display(e);
        },
        done : function(e) {
            console.log("DONE");
            enableSearchButton(true);
        }
    });
}

function enableSearchButton(flag) {
    $("#btn-search").prop("disabled", flag);
}

function display(data) {
    var json = "<h4>Ajax Response</h4>" +
        "<pre>"+
        + JSON.stringify(data, null, 4)
        + "</pre>";
    $('#feedback').html(json);
}