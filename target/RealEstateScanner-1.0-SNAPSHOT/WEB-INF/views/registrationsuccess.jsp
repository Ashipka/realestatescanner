<%@ include file="common/header.jspf"%>
    <title>Registration Confirmation Page</title>
</head>
<body>
<div class="generic-container">
    <%@include file="authheader.jsp" %>

    <div class="alert alert-success lead">
        ${success}
    </div>

    <span class="well floatRight">
            Go to <a href="<c:url value='/list' />">Users List</a>
        </span>
</div>
<%@ include file="common/footer.jspf"%>
