package com.shypkao.springmvc.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "AUCTION")
public class Auction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AUCTION_DATE", nullable = false)
    private Date auctionDate;

    @NotEmpty
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "PROPERTY_ID")
    private String propertyID;

    @NotNull
    @Column(name = "START_PRICE", nullable = false)
    private Long startPrice;

    @NotEmpty
    @Column(name = "URL", nullable = false)
    private String url;

    public Auction() {
    }

    public Auction(Date auctionDate, String description, long startPrice, String url) {
        this.auctionDate = auctionDate;
        this.description = description;
        this.startPrice = startPrice;
        this.url = url;
    }

    @OneToMany(mappedBy = "user", targetEntity=UserAuction.class)
    private Set<Auction> users = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAuctionDate() {
        return auctionDate;
    }

    public void setAuctionDate(Date auctionDate) {
        this.auctionDate = auctionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPropertyID() {
        return propertyID;
    }

    public void setPropertyID(String propertyID) {
        this.propertyID = propertyID;
    }

    public long getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(long startPrice) {
        this.startPrice = startPrice;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<Auction> getUsers() {
        return users;
    }

    public void setUsers(Set<Auction> users) {
        this.users = users;
    }

    /*

    public Set<UserProfile> getUserAuctions() {
        return userAuctions;
    }

    public void setUserAuctions(Set<UserProfile> userAuctions) {
        this.userAuctions = userAuctions;
    }

*/
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((propertyID == null) ? 0 : propertyID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Auction))
            return false;
        Auction other = (Auction) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Auction [id=" + id + ", propertyID=" + propertyID + ", auctionDate=" + auctionDate
                + ", start price=" + startPrice + ", url=" + url + "]";
    }
}
