package com.shypkao.springmvc.service;

import com.shypkao.springmvc.dao.UserAuctionDao;
import com.shypkao.springmvc.model.UserAuction;
import com.shypkao.springmvc.model.UserAuctionPrototype;
import com.shypkao.springmvc.model.utility.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userAuctionService")
@Transactional
public class UserAuctionServiceImpl implements UserAuctionService{

    @Autowired
    private UserAuctionDao dao;

    @Autowired
    private UserService userService;

    public List<UserAuction> findAllAuctionsForUser(String sso, boolean hasAdminRole) {
        return dao.findAllAuctionsForUser(sso, hasAdminRole);
    }

    public List<UserAuctionPrototype> findAuctionsBySearchCriteria(String sso, SearchCriteria search) {
        return dao.findAuctionsBySearchCriteria(sso, search);
    }

    @Override
    public UserAuction createUserAuctionFromPrototype(UserAuctionPrototype userAuctionPrototype) {
        return dao.createUserAuctionFromPrototype(userService.findBySSO(userAuctionPrototype.getSso()), userAuctionPrototype);
    }

    @Override
    public void delete(int id) {
        dao.delete(id);
    }
}
