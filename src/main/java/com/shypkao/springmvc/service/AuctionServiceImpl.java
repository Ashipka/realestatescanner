package com.shypkao.springmvc.service;

import com.shypkao.springmvc.dao.AuctionDao;
import com.shypkao.springmvc.model.Auction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("auctionService")
@Transactional
public class AuctionServiceImpl implements AuctionService {

    @Autowired
    private AuctionDao dao;

    public Auction findById(int id) {
        return dao.findById(id);
    }

    public void saveAuction(Auction auction) {
        dao.save(auction);
    }

    /*
     * Since the method is running with Transaction, No need to call hibernate update explicitly.
     * Just fetch the entity from db and update it with proper values within transaction.
     * It will be updated in db once transaction ends.
     */
    public void updateAuction(Auction auction) {
        Auction entity = dao.findById(auction.getId());
        if(entity!=null){
            entity.setAuctionDate(auction.getAuctionDate());
            entity.setDescription(auction.getDescription());
            entity.setPropertyID(auction.getPropertyID());
            entity.setStartPrice(auction.getStartPrice());
            entity.setUrl(auction.getUrl());
        }
    }

    public void deleteAuctionById(int id) {
        dao.deleteById(id);
    }
}

