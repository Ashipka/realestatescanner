package com.shypkao.springmvc.service;

import com.shypkao.springmvc.model.UserAuction;
import com.shypkao.springmvc.model.UserAuctionPrototype;
import com.shypkao.springmvc.model.utility.SearchCriteria;

import java.util.List;

public interface UserAuctionService {

    List<UserAuction> findAllAuctionsForUser(String sso, boolean hasAdminRole);

    List<UserAuctionPrototype> findAuctionsBySearchCriteria(String sso, SearchCriteria search);

    UserAuction createUserAuctionFromPrototype(UserAuctionPrototype userAuctionPrototype);

    void delete(int id);
}
