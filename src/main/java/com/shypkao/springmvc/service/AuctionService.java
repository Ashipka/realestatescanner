package com.shypkao.springmvc.service;

import com.shypkao.springmvc.model.Auction;


public interface AuctionService {

    Auction findById(int id);

    void saveAuction(Auction auction);

    void updateAuction(Auction auction);

    void deleteAuctionById(int id);

}
