package com.shypkao.springmvc.dao;

import com.shypkao.springmvc.model.Auction;


public interface AuctionDao {

    Auction findById(int id);

    void save(Auction auction);

    void deleteById(int id);
}
