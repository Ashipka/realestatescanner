package com.shypkao.springmvc.dao;

import com.shypkao.springmvc.model.Auction;
import com.shypkao.springmvc.model.User;
import com.shypkao.springmvc.model.UserAuction;
import com.shypkao.springmvc.model.UserAuctionPrototype;
import com.shypkao.springmvc.model.utility.SearchCriteria;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository("userAuctionDao")
public class UserAuctionDaoImpl extends AbstractDao<Integer, UserAuction> implements UserAuctionDao {

    @Autowired
    AuctionDao auctionDao;

    public List<UserAuctionPrototype> findAuctionsBySearchCriteria(String sso, SearchCriteria search) {
        boolean firstPage = true;
        Elements nodesContainUrl = null;
        Map<String, String> cookies = null;
        Connection.Response response = null;
        List<UserAuctionPrototype> userAuctions = new ArrayList<>();
        try {
            do {
                if (firstPage) {
                    response = addRequestAttr(Jsoup.connect(getAuctionUrl(nodesContainUrl))).method(Connection.Method.POST)
                            .data("CategoryId", "30")
                            .data("City", "Warszawa")
                            .data("StartDateFrom", search.getStartDate().replaceAll("-", "."))
                            .data("StartDateTo", search.getEndDate().replaceAll("-", "."))
                            .execute();
                    cookies = response.cookies();
                    firstPage = false;
                } else {
                    response = addRequestAttr(Jsoup.connect(getAuctionUrl(nodesContainUrl))).method(Connection.Method.GET)
                            .cookies(cookies).execute();
                }
                Document parse = response.parse();
                Elements auctionRows = parse.getElementsByTag("tr");
                for (Element auctionRow : auctionRows
                        ) {
                    if (auctionRow.getElementsByTag("th").size() == 0) {
                        Map<String, String> userAuctionMap = new HashMap<>();
                        Elements cells = auctionRow.getElementsByTag("td");
                        String auctionDateStr = cells.get(2).text();
                        String[] auctionDateParts = auctionDateStr.split("\\.");
                        UserAuctionPrototype userAuctionPrototype = new UserAuctionPrototype();
                        userAuctionPrototype.setAuctionDate(new StringBuilder(auctionDateParts[2])
                                .append("-")
                                .append(auctionDateParts[1])
                                .append("-")
                                .append(auctionDateParts[0]).toString());
                        userAuctionPrototype.setAuctionDescription(cells.get(4).text());
                        userAuctionPrototype.setUrl("http://www.licytacje.komornik.pl" + cells.get(7).getElementsByAttribute("href").attr("href"));
                        userAuctionPrototype.setStartPrice(auctionRow.getElementsByTag("td")
                                .get(6).text().replaceAll("zł", "")
                                .replaceAll(" ", "").replaceAll(" ", "")
                                .replaceAll(",", "."));
                        userAuctionPrototype.setSso(sso);
                        userAuctions.add(userAuctionPrototype);
                    }
                }
                nodesContainUrl = parse.getElementsContainingOwnText("Następna");
            } while (nodesContainUrl.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userAuctions;
    }

    public List<UserAuction> findAllAuctionsForUser(String sso, boolean hasAdminRole) {
        Criteria criteria = getSession().createCriteria(UserAuction.class);
        criteria.createAlias("user", "userAlias");
        if (!hasAdminRole) {
            criteria.add(Restrictions.eq("userAlias.ssoId", sso));
        }
        List<UserAuction> auctions = (List<UserAuction>) criteria.list();
        for (UserAuction userAuction : auctions
                ) {
            Hibernate.initialize(userAuction.getAuction());
            Hibernate.initialize(userAuction.getUser());
        }
        return auctions;
    }

    private Connection addRequestAttr(Connection connection) {
        String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:58.0) Gecko/20100101 Firefox/58.0";
        connection
                .header("Host", "www.licytacje.komornik.pl")
                .header("Accept", "text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Accept-Encoding", "gzip, deflate")
                .header("Connection", "keep-alive")
                .userAgent(userAgent);
        return connection;
    }

    private String getAuctionUrl(Elements nodesContainUrl) {
        if (nodesContainUrl == null) {
            return "http://www.licytacje.komornik.pl/Notice/Search";
        } else {
            return "http://www.licytacje.komornik.pl" + nodesContainUrl.attr("href");
        }
    }

    @Override
    public UserAuction createUserAuctionFromPrototype(User user, UserAuctionPrototype userAuctionPrototype) {
        Auction auction = new Auction();
        auction.setDescription(userAuctionPrototype.getAuctionDescription());
        auction.setAuctionDate(Date.valueOf(userAuctionPrototype.getAuctionDate()));
        auction.setStartPrice(Math.round(Float.valueOf(userAuctionPrototype.getStartPrice())));
        auction.setUrl(userAuctionPrototype.getUrl());
        auctionDao.save(auction);

        UserAuction userAuction = new UserAuction();
        userAuction.setUser(user);
        userAuction.setAuction(auction);
        userAuction.setStatus(0);
        persist(userAuction);
        return userAuction;
    }

    @Override
    public void delete(int id) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("id", id));
        UserAuction userAuction = (UserAuction) crit.uniqueResult();
        delete(userAuction);
    }
}
