package com.shypkao.springmvc.dao;

import com.shypkao.springmvc.model.Auction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository("auctionDao")
public class AuctionDaoImpl extends AbstractDao<Integer, Auction> implements AuctionDao {

    static final Logger logger = LoggerFactory.getLogger(AuctionDaoImpl.class);

    public Auction findById(int id) {
        Auction auction = getByKey(id);
        return auction;
    }

    public void save(Auction auction) {
        persist(auction);
    }

    public void deleteById(int id) {
        delete(getByKey(id));
    }

}
