package com.shypkao.springmvc.dao;

import com.shypkao.springmvc.model.User;
import com.shypkao.springmvc.model.UserAuction;
import com.shypkao.springmvc.model.UserAuctionPrototype;
import com.shypkao.springmvc.model.utility.SearchCriteria;

import java.util.List;

public interface UserAuctionDao {
    List<UserAuction> findAllAuctionsForUser(String sso, boolean hasAdminRole);
    List<UserAuctionPrototype> findAuctionsBySearchCriteria(String sso, SearchCriteria search);
    UserAuction createUserAuctionFromPrototype(User user, UserAuctionPrototype userAuctionPrototype);
    void delete(int id);
}
