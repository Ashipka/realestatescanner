<table class="table table-hover">
    <thead>
    <tr>
        <th>Auction date</th>
        <th>Description</th>
        <th>ID</th>
        <th>Start price</th>
        <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
            <th width="100">User</th>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
            <th width="100"></th>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN')">
            <th width="100"></th>
        </sec:authorize>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${userAuctions}" var="userAuction">
        <c:set var="auction" value="${userAuction.auction}"/>
        <tr>
            <td>${auction.auctionDate}</td>
            <td><a href=${auction.url}>${auction.description}</a></td>
            <td>${auction.propertyID}</td>
            <td>${auction.startPrice}</td>
            <td><a href="<c:url value='/edit-user-${userAuction.user.ssoId}' />">${userAuction.user.ssoId}</a></td>
            <sec:authorize access="hasRole('ADMIN')">
                <td><a href="<c:url value='/delete-auction-${userAuction.id}' />" class="btn btn-danger custom-width">delete</a></td>
            </sec:authorize>
        </tr>
    </c:forEach>
    </tbody>
</table>