<%@ include file="common/header.jspf" %>
<title>Auctions List</title>
</head>

<body>
<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
    <%@ include file="common/navigation.jspf" %>
</sec:authorize>
<div class="generic-container">
    <%@include file="authheader.jsp" %>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of auctions </span></div>
        <%@include file="auctionsTable.jsp" %>
    </div>
    <div class="well">
        <a href="<c:url value='/newauction' />">Add new auctions</a>
    </div>
</div>
<%@ include file="common/footer.jspf" %>
