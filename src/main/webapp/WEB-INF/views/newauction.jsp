<%@ include file="common/header.jspf" %>
<sec:csrfMetaTags/>
<title>User Registration Form</title>
</head>

<body>
<div class="generic-container">
    <%@include file="authheader.jsp" %>

    <div class="well lead">Auctions search form</div>
    <form class="form-inline" id="search-form">
        <sec:csrfInput/>
        <div class="form-group">
            <label for="startDate">From</label>
            <div class="form-group">
                <div class="input-group date" id="datetimepicker6">
                    <input class="form-control" type="text" id="startDate">
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="endDate">to</label>
            <div class="form-group">
                <div class="input-group date" id="datetimepicker7">
                    <input class="form-control" type="text" id="endDate">
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </div>
        </div>
        <button type="submit" id="bth-search" class="btn btn-default">Search</button>
    </form>
    <div id="feedback">
        <table class="table table-hover" id="tableData">
            <thead>
            <tr>
                <th>Auction date</th>
                <th>Description</th>
                <th>Start price</th>
                <th class="hidden">User</th>
                <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
                    <th width="100"></th>
                </sec:authorize>
                <sec:authorize access="hasRole('ADMIN')">
                    <th width="100"></th>
                </sec:authorize>
            </tr>
            </thead>
            <tbody id="records_table">
            </tbody>
        </table>
    </div>
    <div>
        <button type="submit" id="bth-add" class="btn btn-default">Add selected</button>
        <button type="submit" id="bth-close" class="btn btn-default">Close</button>
    </div>
</div>
<%@ include file="common/footer.jspf" %>