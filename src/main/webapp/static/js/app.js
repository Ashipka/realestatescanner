jQuery(function () {
    $('#datetimepicker6').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('#datetimepicker7').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#datetimepicker6').datetimepicker();
    $('#datetimepicker7').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });

    document.getElementById("bth-close").onclick = function () {
        location.href = "auctions";
    };

    document.getElementById("bth-add").onclick = function () {
        sendUserSelectedAuctions();
    };
});

function sendUserSelectedAuctions() {
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;

    var result = $("tr:has(:checked)");
    var json = result.map(function () {
        return [$(this).children().slice(0,5).map(function () {
            return $(this).text().trim()
        }).get()]
    }).get();

    var userAuctionPrototypes = [];
    json.forEach(function(entry) {
        var userAuctionPrototype = {};
        userAuctionPrototype["auctionDate"] = entry[0];
        userAuctionPrototype["auctionDescription"] = entry[1];
        userAuctionPrototype["startPrice"] = entry[2];
        userAuctionPrototype["sso"] = entry[3];
        userAuctionPrototype["url"] = entry[4];
        userAuctionPrototypes.push(userAuctionPrototype);
    });

    $.ajax({
        headers: headers,
        url : 'adduserauctions',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data : JSON.stringify(userAuctionPrototypes),
        timeout : 100000,
        success: function (data) {
            location.href = "auctions";
        },
        error : function(e) {
            console.log("ERROR: ", e);
            display(e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}




